let

  name = "nix-shell.dmenu";

  pkgs = import (builtins.fetchTarball {
    url = "https://releases.nixos.org/nixos/21.11/nixos-21.11.336020.2128d0aa28e/nixexprs.tar.xz";
    sha256 = "0w8plbxms0di6gnh0k2yhj0pgxzxas7g5x0m01zjzixf16i2bapj"; }) {};

  mkShellMinimal = pkgs.callPackage (builtins.fetchurl {
    url = "https://raw.githubusercontent.com/tdro/dotfiles/0aac4414559235f8cd8c454acce30c0471e0f6b1/.config/nixpkgs/helpers/mkShellMinimal.nix";
    sha256 = "06vbyyhaam3nmzimzasz6la590ni9cbdql3jy29hhw9ln0xf09yy";
  }) { };

in mkShellMinimal rec {
  buildInputs = [
    pkgs.bash
    pkgs.coreutils
    pkgs.gcc
    pkgs.git
    pkgs.gnumake
    pkgs.xlibs.libX11.dev
    pkgs.xlibs.libXft.dev
    pkgs.xlibs.libXinerama.dev
  ];
  shellHook = ''
    export PS1='\h (${name}) \W \$ '
  '';
}
